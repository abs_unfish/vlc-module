/**
 * @file goalbit_broadcaster.c
 * @brief Hello world interface VLC module example
 */
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdlib.h>
/* VLC core API headers */
#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_sout.h>
#include <vlc_block.h>

#include <goalbit-client/goalbit.h>

/* Forward declarations */
static int Open(vlc_object_t *);
static void Close(vlc_object_t *);

/* Module descriptor */
vlc_module_begin()
    set_shortname(N_("GB stream output"))
    set_description(N_("Hello interface"))
    set_capability("sout access", 0)
    add_shortcut("gb-broadcaster")
    set_callbacks(Open, Close)
    set_category(CAT_SOUT)
    set_subcategory( SUBCAT_SOUT_ACO )
    add_string("hello-who", "world", NULL, "Target", "Whom to say hello to.", false)
    //add_string( "sout-gbroadcaster-*", "channel-name", "", NULL, CHANNEL_NAME_TEXT, CHANNEL_NAME_LONGTEXT, true )
vlc_module_end ()


typedef struct sout_access_out_sys_t
{
    /* tracker_url */
    char* tracker_url;
    /* channel_identifier */
    char* channel_identifier;
    goalbit_param_t* param;
    goalbit_t* goalbit;

    /* gather header from stream */
    int                 i_header_allocated;
    int                 i_header_size;
    uint8_t             *p_header;
    bool          b_header_complete;
} vlc_gb_broadcast_state_t;



static ssize_t Write( sout_access_out_t *, block_t * );
static int Seek ( sout_access_out_t *, off_t  );
static int Control( sout_access_out_t *, int, va_list );

static void free_vlc_gb_broadcast_state(vlc_gb_broadcast_state_t** ptr)
{
    vlc_gb_broadcast_state_t* p_state = *ptr;

    if (p_state->tracker_url)
        free(p_state->tracker_url);
    if (p_state->channel_identifier)
        free(p_state->channel_identifier);

    if (p_state->p_header)
        free(p_state->p_header);

    if (p_state->goalbit)
        goalbit_client_close(p_state->goalbit);
    if (p_state->param)
        free(p_state->param);

    free(p_state);
    *ptr = NULL;
}

static void * event_callback( goalbit_t *p_this, goalbit_event_t *p_event )
{
    log_debug("event_callback - event %d", p_event->i_event);

    if ( p_event->i_event == EVENT_BUFF_UPDATE )
    {
        log_debug( "BUFFERING......%d%c\n", p_event->g_value.i_int, '%' );
    }
    else if ( p_event->i_event == EVENT_TRACKER_COM )
    {
        log_debug( "Conected to Tracker\n" );
    }
    else if ( p_event->i_event == EVENT_UPDATE_PEERS_COUNT )
    {
        log_debug( "Conected peers: %d\n", p_event->g_value.i_int );
    }
    else if ( p_event->i_event == EVENT_START_EXEC )
    {
        log_debug( "\n\n\n\nStarting execution...\n\n\n" );
    }
    else if ( p_event->i_event == EVENT_EXEC_UPDATE )
    {
        log_debug( "\n\n\n\nABI: %d\n\n\n", p_event->g_value.i_int );
    }
    else
    {
        log_debug( "\n\n\n\nEvent %d received....\n\n\n", p_event->i_event );
    }
    return NULL;
}




/**
 * Starts our example interface.
 */
static int Open(vlc_object_t *obj)
{
    sout_access_out_t       *p_access = (sout_access_out_t*)obj;
    vlc_gb_broadcast_state_t   *p_sys;
    char* tracker_url = NULL, *channel_name = NULL, *path, *ptr;
    int index = 0;
    char buffer[1000];



    log_debug("gB broadcaster %s", p_access->psz_path);
    snprintf(buffer, 1000, "%s", p_access->psz_path);

    path = buffer;

    log_debug("path: %s", path);
    //assert(path[0] == ':');

    if ((ptr = strchr(path + 1, ':')) != NULL)
    {
        log_debug("found ptr: %s", ptr);
        *ptr = '\0';
        tracker_url = strdup(ptr+1);
        channel_name = strdup(path+1);
    }


    if( !( p_sys = p_access->p_sys =  (sout_access_out_sys_t*) calloc(1, sizeof( vlc_gb_broadcast_state_t ) ) ) )
        return VLC_ENOMEM ;

    p_sys->channel_identifier = channel_name;
    p_sys->tracker_url = tracker_url;


    goalbit_param_t  *p_param = (goalbit_param_t*) calloc(1, sizeof(goalbit_param_t));
    goalbit_t*        p_goalbit;

    memset(p_param, 0, sizeof(goalbit_param_t));

    goalbit_broadcaster_param(p_param,
                              p_sys->tracker_url,
                              p_sys->channel_identifier
        );


    p_param->pf_event_callback = event_callback;

    log_debug( "START..............................." );
    p_goalbit = goalbit_client_open( p_param );

    if ( !p_goalbit )
    {
        log_err( "ERROR when openning libgoalbit library\n\n\n" );
        return VLC_EGENERIC;
    }

    goalbit_param_print( p_goalbit, p_param );    

    p_sys->i_header_allocated = 1024;
    p_sys->i_header_size      = 0;
    p_sys->p_header           = (uint8_t*) xmalloc( p_sys->i_header_allocated );
    p_sys->b_header_complete  = false;
    p_sys->param = p_param;
    p_sys->goalbit = p_goalbit;

    p_access->pf_write       = Write;
    p_access->pf_seek        = Seek;
    p_access->pf_control     = Control;

    p_access->p_sys = p_sys;

    msg_Info(p_access, "Hello s!");
    return VLC_SUCCESS;
}

/**
 * Stops the interface.
 */
static void Close(vlc_object_t *obj)
{
    sout_access_out_t       *p_access = (sout_access_out_t*)obj;
    //vlc_gb_broadcast_state_t   *p_state = p_access->p_sys;

    msg_Info(p_access, "Good bye s!");
#if 0
    if (p_state)
    {
        goalbit_client_close(p_state->goalbit);
        
        free(p_state->tracker_url);
        free(p_state->channel_identifier);
        free(p_state->param);
        free(p_state->goalbit);

        free(p_state);
        p_access->p_sys = NULL;
        
    }
#endif
    free_vlc_gb_broadcast_state(&p_access->p_sys);
}

/*****************************************************************************
 * Seek: seek to a specific location in a file
 *****************************************************************************/
static int Seek( sout_access_out_t *p_access, off_t i_pos )
{
    log_debug("GB sout access cannot seek" );
    return VLC_EGENERIC;
}

/*****
************************************************************************
 * Write:
 *****************************************************************************/
static ssize_t Write( sout_access_out_t *p_access, block_t *p_buffer )
{
    //log_debug("GB out access cannot write");

    vlc_gb_broadcast_state_t   *p_sys = p_access->p_sys;
    int i_err = 0;
    int i_len = 0;


    while (p_buffer)
    {
        block_t *p_next;

        if( p_buffer->i_flags & BLOCK_FLAG_HEADER )
        {
            /* gather header */
            if( p_sys->b_header_complete )
            {
                /* free previously gathered header */
                p_sys->i_header_size = 0;
                p_sys->b_header_complete = false;
            }
            if( (int)(p_buffer->i_buffer + p_sys->i_header_size) >
                p_sys->i_header_allocated )
            {
                p_sys ->i_header_allocated =
                    p_buffer->i_buffer + p_sys->i_header_size + 1024;
                p_sys->p_header = (uint8_t*) xrealloc( p_sys->p_header, p_sys->i_header_allocated );
            }
            memcpy( &p_sys->p_header[p_sys->i_header_size],
                    p_buffer->p_buffer,
                    p_buffer->i_buffer );
            p_sys->i_header_size += p_buffer->i_buffer;
        }        
        else if( !p_sys->b_header_complete )
        {
            p_sys->b_header_complete = true;
            
        }

        i_len += p_buffer->i_buffer;
        /* send data */
#if 0
        i_err = httpd_StreamSend( p_sys->p_httpd_stream, p_buffer->p_buffer,
                                  p_buffer->i_buffer );
#endif

        //sign data


        i_err = goalbit_client_write(p_sys->goalbit, (char*)p_buffer->p_buffer, p_buffer->i_buffer);


        p_next = p_buffer->p_next;
        block_Release( p_buffer );
        p_buffer = p_next;

        if( i_err < 0 )
        {
            break;
        }
    }

    if( i_err < 0 )
    {
        block_ChainRelease( p_buffer );
    }

    return VLC_EGENERIC;
}
/***********************
 * Control
 *************************/

static int Control( sout_access_out_t *p_access, int i_query, va_list args )
{
    log_debug("control %d", i_query);
    (void)p_access;


    switch( i_query )
    {
        case ACCESS_OUT_CONTROLS_PACE:
            *va_arg( args, bool * ) = false;
            break;

        default:
            return VLC_EGENERIC;
    }
    return VLC_SUCCESS;
}
