#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


#include <vlc_common.h>
#include <vlc_plugin.h>

#include <vlc_access.h>
#include <vlc_url.h>

//#include "my_macro.h"

#include <goalbit-client/goalbit.h>
#include <goalbit-client/goalbit_helpers.h>
//#include <goalbit-client/basetools/block.h>

static int  Open ( vlc_object_t * );
static void Close( vlc_object_t * );

vlc_module_begin ()
    set_description( N_("goalbit input") )
    set_capability( "access", 0 )
    set_shortname( N_( "gbt" ) )
    set_category( CAT_INPUT )
    set_subcategory( SUBCAT_INPUT_ACCESS )

    add_shortcut( "gbt" )

    set_callbacks( Open, Close )
vlc_module_end ()


struct access_sys_t
{
    char* tracker_url;
    char* channel_id;
    goalbit_param_t* param;
    goalbit_t* goalbit;
} vlc_dcast_peer_state;

//static ssize_t Read( access_t *, uint8_t *, size_t );
static block_t* Block( access_t *);
static int Seek( access_t *, uint64_t );
static int Control( access_t *, int, va_list );

static void * event_callback( goalbit_t *p_this, goalbit_event_t *p_event )
{
    log_debug("event_callback - event %d", p_event->i_event);

    if ( p_event->i_event == EVENT_BUFF_UPDATE )
    {
        log_debug( "BUFFERING......%d%c\n", p_event->g_value.i_int, '%' );
    }
    else if ( p_event->i_event == EVENT_TRACKER_COM )
    {
        log_debug( "Conected to Tracker\n" );
    }
    else if ( p_event->i_event == EVENT_UPDATE_PEERS_COUNT )
    {
        log_debug( "Conected peers: %d\n", p_event->g_value.i_int );
    }
    else if ( p_event->i_event == EVENT_START_EXEC )
    {
        log_debug( "\n\n\n\nStarting execution...\n\n\n" );
    }
    else if ( p_event->i_event == EVENT_EXEC_UPDATE )
    {
        log_debug( "\n\n\n\nABI: %d\n\n\n", p_event->g_value.i_int );
    }
    else
    {
        log_debug( "\n\n\n\nEvent %d received....\n\n\n", p_event->i_event );
    }
    return NULL;
}



static int Open(vlc_object_t* p_this)
{
    access_t *p_access = (access_t*)p_this;
    access_sys_t *p_sys = NULL;
    goalbit_t*        p_goalbit = NULL;
    goalbit_param_t*  p_param;

    STANDARD_BLOCK_ACCESS_INIT;

    if(!( p_sys = p_access->p_sys = (access_sys_t*) calloc(1, sizeof(access_sys_t ))))
        return VLC_ENOMEM ;


    log_debug("@@@ ACCESS INPUT Open, %s", p_access->psz_path);
    parse_dcast_url(p_access->psz_path, &p_sys->tracker_url, &p_sys->channel_id);

    log_debug("@@@ tracker: %s, channel: %s", p_sys->tracker_url, p_sys->channel_id);

    p_param = (goalbit_param_t*)malloc( sizeof(goalbit_param_t) );
    goalbit_param_default( p_param );
    // FIXME remove strdup
    p_param->psz_tracker_url = p_sys->tracker_url;
    p_param->psz_channel_name = p_sys->channel_id;
    p_param->psz_base_dir = strdup("/tmp/libgoalbit/vlc_downloader_dir");

    log_debug( "START..............................." );
    p_goalbit = goalbit_client_open( p_param );

    log_debug("p_goalbit: %s", p_goalbit);
    goalbit_param_print( p_goalbit, p_param );

    p_sys->goalbit = p_goalbit;
    p_sys->param = p_param;
    
    return VLC_SUCCESS;
}

static block_t* Block(access_t* p_access)
{
    typedef struct
    {
        size_t      i_buffer_size;
        size_t      i_buffer_index;
        uint8_t     *p_buffer;
    } goalbit_block_t;

    goalbit_block_t *p_chunk = NULL; 
    int i_status = 0;
    access_sys_t *p_sys = p_access->p_sys;

    p_chunk = goalbit_client_read( p_sys->goalbit, &i_status );


    if ( i_status == READ_NOK_DIEING || i_status == READ_NOK_EOS )
    {
        log_debug( "No more content to read!\n" );
        return NULL;
    }
    else if ( i_status == READ_NOK_SYNCING )
    {
        //There's not a piece ready to be consumed
        return NULL;
    }
    else if ( i_status == READ_NOK_BUFFERING )
    {
        //sleep( 1 );
        return NULL;
    }
    else if ( i_status == READ_OK )
    {
        log_debug("OK, read chunk %p", p_chunk);
        if (p_chunk)
        {
            log_debug("chunk - size %u, index %u, buffer %p",
                      p_chunk->i_buffer_size,
                      p_chunk->i_buffer_index,
                      p_chunk->p_buffer
                );

            block_t* vlc_block = block_New(p_access, p_chunk->i_buffer_index);
            memcpy(vlc_block->p_buffer, p_chunk->p_buffer, p_chunk->i_buffer_index);
            vlc_block->i_buffer = p_chunk->i_buffer_index;
            
            log_debug("releasing p_chunk");
            goalbit_block_release( &p_chunk );

            return vlc_block;
        }else
        {
            log_err("NULL p_chunk when READ_OK status");
            return NULL;
        }
    }

    log_debug("@@@ ACCESS input end");

    return NULL;
}

static ssize_t Read( access_t* p_access, uint8_t* p_buffer, size_t i_len)
{
    log_debug("@@@ ACCESS INPUT Read");

    return VLC_SUCCESS;
}


void Close(vlc_object_t* p_this)
{
    access_t *p_access = (access_t*)p_this;
    access_sys_t *p_sys = p_access->p_sys;

    goalbit_client_close(p_sys->goalbit);

}

static int Control( access_t *p_access, int i_query, va_list args )
{
    access_sys_t *p_sys = p_access->p_sys;
    bool       *pb_bool;
    int64_t    *pi_64;
    vlc_meta_t *p_meta;

    switch( i_query )
    {
        /* */
        default:
            msg_Warn( p_access, "unimplemented query in control" );
            return VLC_EGENERIC;

    }
    return VLC_SUCCESS;
}

static int Seek( access_t *p_access, uint64_t i_pos )
{
    msg_Dbg( p_access, "trying to seek to %"PRId64, i_pos );
    return VLC_SUCCESS;
}
